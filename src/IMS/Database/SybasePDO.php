<?php

namespace IMS\Database;

use PDO;
use PDOException;

class SybasePDO
{
    protected $dbHost = '';
    protected $dbName = '';
    protected $dbUser = '';
    protected $dbPass = '';
    protected $logger;
    protected $pdo = null;
    protected $stmt;
    protected $data = [];
    
    public function __construct(\Monolog\Logger $logger) {
        $this->logger = $logger;
        $this->connect();
    }
    
    public function connect()
    {
        try {
            $dsn = "dblib:host={$this->dbHost};dbname={$this->dbName}";
            $this->pdo = new PDO($dsn, $this->dbName, $this->dbPass);
        } catch (PDOException $e) {
            $this->logger->addError($e->getMessage());
        }
        
    }
    
    public function selectDB($db)
    {
        $this->dbName = $db;
        try {
            return $this->pdo->exec("USE {$this->dbName}"); 
        } catch(PDOException $e) {
            $this->logger->addError($e->getMessage());
        }
    }
    
    public function getColumns($table)
    {
        $columns = array();
        $this->stmt = $this->pdo->query("select * from {$table} where 1 = 2");
        $columnCount = $this->stmt->columnCount();
        for ($i = 0; $i < $columnCount; $i++) {
            $columns[] = $this->stmt->getColumnMeta($i)['name'];
        }
        return $columns;
    }
    
    public function query($sql, $fetchType = 'ASSOC')
    {
        $this->data = [];
        
        try {
            $this->stmt = $this->pdo->prepare($sql);
            $this->stmt->execute();
            $this->fetchType($fetchType);
        } catch(PDOException $e) {
            $this->logger->addError($e->getMessage());
        }
        
        return $this->data;
    }
    
    private function fetchType($type)
    {        
        switch($type) {
            case 'ASSOC':
                while ($row = $this->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
                break;
            case 'OBJ':
                while ($row = $this->stmt->fetch(PDO::FETCH_OBJ)) {
                    $this->data[] = $row;
                }
                break;
            default:
                while ($row = $this->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
        }
    }
    
    public function getPDO()
    {
        return $this->pdo;
    }
    
}